﻿using Core.Domain.Models;
using Data.Persistance.Context;
using Data.Persistance.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Persistance.Repository
{
    public class UserInfoRepository : IUserInfoRepository
    {
        private readonly DefaultContext _context;

        public UserInfoRepository(DefaultContext context)
        { 
            _context = context;
        }

        public async Task<UserInfo?> GetById(int id)
        {
            using (_context)
            {
                return await _context.UserInfo.FirstOrDefaultAsync(x =>x.Id == id);
            }
        }

        public async Task Save(UserInfo userInfo)
        {
            using (_context)
            {
                _context.UserInfo.Add(userInfo);
                await _context.SaveChangesAsync();
            }
        }
    }
}
