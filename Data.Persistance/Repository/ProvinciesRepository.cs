﻿using Core.Domain.Models;
using Data.Persistance.Context;
using Data.Persistance.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Persistance.Repository
{
    public class ProvinciesRepository : IProvinciesRepository
    {
        private readonly DefaultContext _context;

        public ProvinciesRepository(DefaultContext context)
        {
            _context = context;
        }

        public async Task<Province?> Get(int id)
        {
            return await _context.Provinces.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Province>?> GetAll()
        {
            return await _context.Provinces.ToListAsync();
        }

        public async Task<IEnumerable<Province>?> GetByCountry(int countryId)
        {
            return await _context.Provinces.Where(x => x.CountryId == countryId).ToListAsync();
        }


    }
}
