﻿using Core.Domain.Models;
using Data.Persistance.Context;
using Data.Persistance.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Persistance.Repository
{
    public class CountriesRepository : ICountriesRepository
    {
        private readonly DefaultContext _context;

        public CountriesRepository(DefaultContext context)
        {
            _context = context;
        }

        public async Task<Country?> Get(int id)
        {
            return await _context.Countries.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Country>?> GetAll()
        {
            return await _context.Countries.ToListAsync();
        }
    }
}
