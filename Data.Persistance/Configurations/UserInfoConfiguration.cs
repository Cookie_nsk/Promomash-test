﻿using Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Persistance.Configurations
{
    public class UserInfoConfiguration : IEntityTypeConfiguration<UserInfo>
    {
        public void Configure(EntityTypeBuilder<UserInfo> builder)
        {
            builder.ToTable("UserInfo");
            builder.HasKey(s => s.Id);

            builder.
            Property(p => p.Email)
                    .HasColumnName("Email")
                    .HasColumnType("nvarchar")
                    .HasMaxLength(100);

            builder.
            Property(p => p.Password)
                    .HasColumnName("Password")
                    .HasColumnType("nvarchar")
                    .HasMaxLength(500);

            builder.
            Property(p => p.CountryId)
                    .HasColumnName("CountryId")
                    .HasColumnType("int");

            builder.
            Property(p => p.ProvinceId)
                    .HasColumnName("ProvinceId")
                    .HasColumnType("int");

            builder.
                HasOne(x => x.Country)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);

            builder.
                HasOne(x => x.Province)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);


        }
    }
}
