﻿using Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Persistance.Configurations
{
    public class ProvinceConfiguration : IEntityTypeConfiguration<Province>
    {
        public void Configure(EntityTypeBuilder<Province> builder)
        {
            builder.ToTable("Provincies");
            builder.HasKey(s => s.Id);

            builder.
            Property(p => p.Name)
                    .HasColumnName("Name")
                    .HasColumnType("nvarchar")
                    .HasMaxLength(100);

            builder.
            Property(p => p.CountryId)
                    .HasColumnName("CountryId")
                    .HasColumnType("int");
        }
    }
}
