﻿using Core.Domain.Models;
using Data.Persistance.Configurations;
using Data.Persistance.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Data.Persistance.Context
{
    public class DefaultContext : DbContext
    {
#pragma warning disable CS8618
        public DefaultContext(DbContextOptions<DefaultContext> options) : base(options)
        {

        }
#pragma warning restore CS8618

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new UserInfoConfiguration().Configure(modelBuilder.Entity<UserInfo>());
            new CountryConfiguration().Configure(modelBuilder.Entity<Country>());
            new ProvinceConfiguration().Configure(modelBuilder.Entity<Province>());

           modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }

        // Entities        
        public DbSet<UserInfo> UserInfo { get; set; }

        public DbSet<Province> Provinces { get; set; }

        public DbSet<Country> Countries { get; set; }

    }
}
