﻿using Core.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Data.Persistance.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().HasData(
                new Country() { Id = 1, Name = "USA"},
                new Country() { Id = 2, Name = "Canada" },
                new Country() { Id = 3, Name = "Sweden" }
            );

            modelBuilder.Entity<Province>().HasData(
                new Province() { Id = 1, CountryId = 1, Name = "USA Province 0" },
                new Province() { Id = 2, CountryId = 1, Name = "USA Province 1" },
                new Province() { Id = 3, CountryId = 1, Name = "USA Province 2" },
                new Province() { Id = 4, CountryId = 2, Name = "Canada Province 0" },
                new Province() { Id = 5, CountryId = 2, Name = "Canada Province 1" },
                new Province() { Id = 6, CountryId = 2, Name = "Canada Province 2" },
                new Province() { Id = 7, CountryId = 3, Name = "Sweden Province 0" },
                new Province() { Id = 8, CountryId = 3, Name = "Sweden Province 1" },
                new Province() { Id = 9, CountryId = 3, Name = "Sweden Province 2" }
            );

        }
    }
}
