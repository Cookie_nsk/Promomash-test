﻿using Core.Domain.Models;

namespace Data.Persistance.Interfaces
{
    public interface IUserInfoRepository
    {
        Task Save(UserInfo userInfo);

        Task<UserInfo?> GetById(int id);
    }
}
