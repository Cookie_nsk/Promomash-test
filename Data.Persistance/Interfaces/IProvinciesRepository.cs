﻿using Core.Domain.Models;

namespace Data.Persistance.Interfaces
{
    public interface IProvinciesRepository
    {
        Task<Province?> Get(int id);

        Task<IEnumerable<Province>?> GetAll();

        Task<IEnumerable<Province>?> GetByCountry(int countryId);
    }
}
