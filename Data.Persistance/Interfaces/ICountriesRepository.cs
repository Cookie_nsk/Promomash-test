﻿using Core.Domain.Models;

namespace Data.Persistance.Interfaces
{
    public interface ICountriesRepository
    {
        Task<Country?> Get(int id);

        Task<IEnumerable<Country>?> GetAll();
    }
}
