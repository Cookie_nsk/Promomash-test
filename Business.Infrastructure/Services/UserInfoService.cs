﻿using AutoMapper;
using Business.Infrastructure.Dtos;
using Business.Infrastructure.Interfaces;
using Core.Domain.Models;
using Data.Persistance.Interfaces;
using System.Text;

namespace Business.Infrastructure.Services
{
    public class UserInfoService : IUserInfoService
    {
        private readonly IUserInfoRepository _userRepo;
        private readonly IMapper _mapper;

        public UserInfoService(IUserInfoRepository userRepo, IMapper mapper)
        {
            _userRepo = userRepo;
            _mapper = mapper;
        }

        public async Task<UserInfoDto> GetById(int id)
        {
            return _mapper.Map<UserInfoDto>(await _userRepo.GetById(id));
        }

        public async Task Save(UserInfoDto userInfoDto)
        {
            //Here some strong password encoding needs to be implemented
            userInfoDto.Password = Convert.ToBase64String(Encoding.UTF8.GetBytes(userInfoDto.Password));

            await _userRepo.Save(_mapper.Map<UserInfo>(userInfoDto));
        }
    }
}
