﻿using AutoMapper;
using Business.Infrastructure.Dtos;
using Business.Infrastructure.Interfaces;
using Data.Persistance.Interfaces;

namespace Business.Infrastructure.Services
{
    public class RegionalService : IRegionalService
    {
        private readonly ICountriesRepository _countryRepo;
        private readonly IProvinciesRepository _provRepo;
        private readonly IMapper _mapper;

        public RegionalService(ICountriesRepository countryRepo,
            IProvinciesRepository provinciesRepo, IMapper mapper)
        {
            _countryRepo = countryRepo;
            _provRepo = provinciesRepo;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CountryDto>> GetCountries()
        {
            return _mapper.Map<IEnumerable<CountryDto>>(await _countryRepo.GetAll());
        }

        public async Task<IEnumerable<ProvinceDto>> GetProvinciesByCountry(int countryId)
        {
            return _mapper.Map<IEnumerable<ProvinceDto>>(await _provRepo.GetByCountry(countryId));
        }
    }
}
