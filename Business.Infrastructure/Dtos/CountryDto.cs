﻿namespace Business.Infrastructure.Dtos
{
    public class CountryDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
