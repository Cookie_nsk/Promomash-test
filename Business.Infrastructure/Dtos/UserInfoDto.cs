﻿namespace Business.Infrastructure.Dtos
{
    public class UserInfoDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Agreement { get; set; }
        public string Country { get; set; }
        public string Province { get; set; }
    }
}
