﻿namespace Business.Infrastructure.Dtos
{
    public class ProvinceDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
