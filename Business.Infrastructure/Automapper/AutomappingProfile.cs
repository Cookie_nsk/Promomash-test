﻿using AutoMapper;
using Business.Infrastructure.Dtos;
using Core.Domain.Models;

namespace TestAngular.Automapper
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<UserInfoDto, UserInfo>()
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.Country))
                .ForMember(dest => dest.ProvinceId, opt => opt.MapFrom(src => src.Province))
                .ForMember(dest => dest.Country, opt => opt.Ignore())
                .ForMember(dest => dest.Province, opt => opt.Ignore())
                .ReverseMap()
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.CountryId))
                .ForMember(dest => dest.Province, opt => opt.MapFrom(src => src.ProvinceId));

            CreateMap<ProvinceDto, Province>()
                .ForMember(dest => dest.Country, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<CountryDto, Country>()
                .ForMember(dest => dest.Provincies, opt => opt.Ignore())
                .ReverseMap();
                
        }
    }
}
