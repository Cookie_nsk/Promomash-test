﻿using Business.Infrastructure.Dtos;

namespace Business.Infrastructure.Interfaces
{
    public interface IUserInfoService
    {
        Task Save(UserInfoDto userInfo);

        Task<UserInfoDto> GetById(int id);
    }
}
