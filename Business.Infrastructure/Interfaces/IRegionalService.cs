﻿using Business.Infrastructure.Dtos;

namespace Business.Infrastructure.Interfaces
{
    public interface IRegionalService
    {
        Task<IEnumerable<CountryDto>> GetCountries();

        Task<IEnumerable<ProvinceDto>> GetProvinciesByCountry(int countryId);
    }
}
