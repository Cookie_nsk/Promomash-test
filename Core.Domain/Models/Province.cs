﻿namespace Core.Domain.Models
{
    public class Province : BaseEntity
    {
        public int CountryId { get; set; }

        public string? Name { get; set; }

        public Country? Country { get; set; }
    }
}
