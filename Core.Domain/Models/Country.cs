﻿namespace Core.Domain.Models
{
    public class Country : BaseEntity
    {
        public string? Name { get; set; }

        public IEnumerable<Province>? Provincies { get; set; }
    }
}
