﻿namespace Core.Domain.Models
{
    public class UserInfo : BaseEntity
    {
        public string Email { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public bool Agreement { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }

        public Province? Province { get; set; }
        public Country? Country { get; set; }
    }
}
