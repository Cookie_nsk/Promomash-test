﻿using Business.Infrastructure.Dtos;
using Business.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TestAngular.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UserDataController : ControllerBase
    {
        private readonly IUserInfoService _userService;
        public UserDataController(IUserInfoService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UserInfoDto payload)
        {
            await _userService.Save(payload);
            return Ok();
        }
    }
}
