﻿using Business.Infrastructure.Dtos;
using Business.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestAngular.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class RegionController : ControllerBase
    {
        private readonly IRegionalService _regionService;

        public RegionController(IRegionalService regionService)
        {
            _regionService = regionService;
        }

        [HttpGet]
        [ActionName("GetCountries")]
        public async Task<IEnumerable<CountryDto>> GetCountries()
        {
            return await _regionService.GetCountries();
        }

        [HttpGet]
        [ActionName("GetProvincies/{id}")]
        public async Task<IEnumerable<ProvinceDto>> GetProvincies([FromRoute] int id)
        {
            return await _regionService.GetProvinciesByCountry(id);
        }


    }
}
