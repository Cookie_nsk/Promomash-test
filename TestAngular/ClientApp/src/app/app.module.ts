import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { LoginPageComponent } from './LoginPage/LoginPage.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FetchDataService } from './fetch-data/fetch-data.service';
import { PostDataService } from './fetch-data/post-data.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    LoginPageComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: LoginPageComponent, pathMatch: 'full' },
    ])
  ],
    providers: [FetchDataService, PostDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
