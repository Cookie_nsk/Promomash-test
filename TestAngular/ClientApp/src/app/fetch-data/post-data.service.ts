import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInfo } from '../Models/UserInfo';

@Injectable()
export class PostDataService {
  _baseUrl: string;
  _client: HttpClient;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._client = http;
    this._baseUrl = baseUrl;
  }

  public createUser(userInfo: UserInfo): Observable<any> {
    const headers = { 'content-type': 'application/json' }
    const body = JSON.stringify(userInfo);
    
    return this._client.post(this._baseUrl + 'api/userdata/create', body, { 'headers': headers });
  }
}

