import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Country } from "../Models/Country";
import { Province } from "../Models/Province";
import { Observable } from 'rxjs';

@Injectable()
export class FetchDataService {
  _baseUrl: string;
  _client: HttpClient;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._client = http;
    this._baseUrl = baseUrl;
  }

  public getCountries(): Observable<Country[]> {
    return this._client.get<Country[]>(this._baseUrl + 'api/region/GetCountries');
  }

    public getProvincies(countryId: string): Observable<Province[]> {
    return this._client.get<Province[]>(this._baseUrl + 'api/region/GetProvincies/' + countryId);
  }
}

