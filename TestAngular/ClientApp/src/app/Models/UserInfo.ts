
export class UserInfo {
  public Email: string;
  public Password: string;
  public Agreement: boolean;
  public Country: string;
  public Province: string;
}
