import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { PostDataService } from '../fetch-data/post-data.service';
import { FetchDataService } from '../fetch-data/fetch-data.service';
import { Country } from '../Models/Country';
import { Province } from '../Models/Province';
import { UserInfo } from '../Models/UserInfo';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
    selector: 'app-step1',
    templateUrl: './LoginPage.component.html',
    styleUrls: ['./LoginPage.component.css']
})

export class LoginPageComponent {
    step1: FormGroup;
    step2: FormGroup;
    submitted = false;
    submitted2 = false;
    showS1 = true;
    userInfo: UserInfo;

    countries: Country[] = [];
    provincies: Province[] = [];
    private subscriptions = new Subscription();

    constructor(private formBuilder: FormBuilder,
        private fetchService: FetchDataService,
        private postService: PostDataService) { }

    get formControls() { return this.step1.controls; }

    get formControls2() { return this.step2.controls; }

    ngOnInit() {

        //Validation setup step 1
        this.step1 = this.formBuilder.group({
            login: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/)]],
            confirmpassword: ['', Validators.required],
            agreementCbx: [false, Validators.requiredTrue]
        }, {
            validators: (control) => {
                if (control.value.password !== control.value.confirmpassword) {
                    control.get("confirmpassword").setErrors({ different: true });
                }
                return null;
            }
        });

        //Validation setup step 2
        this.step2 = this.formBuilder.group({
            ddlCountry: ['', Validators.required],
            ddlProvince: ['', Validators.required]
        });


        this.countries.push(new Country("", "Select country"));
        this.provincies.push(new Province("", "Select province"));

        this.subscriptions.add(this.fetchService.getCountries()
            .subscribe(result => {
                result.forEach(x => this.countries.push(new Country(x.id.toString(), x.name)));
                this.userInfo.Country = "";

            }, error => { alert("Internal server error"); console.error(error) }));

        this.userInfo = new UserInfo();
        this.userInfo.Province = "";
    }

    ngOnDestroy() {
        this.subscriptions.unsubscribe();
    }

    onSubmitS1() {

        this.markFormTouched(this.step1);

        this.submitted = true;

        if (this.step1.invalid) {
            return;
        }

        this.showS1 = false;
    }

    onSubmitS2() {

        this.markFormTouched(this.step2);

        this.submitted2 = true;

        if (this.step2.invalid) {
            return;
        }
        //Create user
        this.subscriptions.add(this.postService.createUser(this.userInfo).subscribe(result => {
            alert('User created');
        }, error => { alert("Internal server error"); console.error(error) }));
    }

    onCountryChanged() {

        this.subscriptions.add(this.fetchService.getProvincies(this.userInfo.Country)
            .subscribe(result => {
                this.provincies = [];
                this.provincies.push(new Province("", "Select province"));

                result.forEach(x => this.provincies.push(new Province(x.id.toString(), x.name)));
                this.userInfo.Province = "";

            }, error => { alert("Internal server error"); console.error(error) }));
    }

    markFormTouched(group: FormGroup | FormArray) {
        Object.keys(group.controls).forEach((key: string) => {
            const control = group.controls[key];
            if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control); }
            else { control.markAsTouched(); };
        });
    };
}
